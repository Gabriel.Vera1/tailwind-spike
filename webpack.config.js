const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path')
const { getLocalIdent } = require('@dr.pogodin/babel-plugin-react-css-modules/utils');
const { generateScopedNameFactory } = require('@dr.pogodin/babel-plugin-react-css-modules/utils');

module.exports = {
  entry: path.resolve(__dirname, 'src/index.js'),
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
    libraryTarget: 'umd',
    libraryExport: 'default' 
  },
  plugins: [new MiniCssExtractPlugin({ filename: "index.module.css" })],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-react', '@babel/preset-env'],
            plugins: [
               ["@dr.pogodin/react-css-modules", {
                  "generateScopedName": generateScopedNameFactory("[path]___[name]__[local]___[hash:base64:5]"),
               }]

            ]
          },
      }},
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: "css-loader",
            options: {
              esModule: true,
              modules: {
                namedExport: true,
                getLocalIdent,
                localIdentName: "[path]___[name]__[local]___[hash:base64:5]",
              },
            },
      },
    ]
  }
    ]
},
}

