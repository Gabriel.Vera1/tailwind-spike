"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _getClassName2 = _interopRequireDefault(require("@dr.pogodin/babel-plugin-react-css-modules/dist/browser/getClassName"));

var _react = _interopRequireDefault(require("react"));

var _indexModule = _interopRequireDefault(require("./index.module.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _styleModuleImportMap = {
  "css": {
    "test": "src-___index-module__test___zmSY5"
  }
};

var Comp = function Comp() {
  return /*#__PURE__*/_react["default"].createElement("button", {
    className: (0, _getClassName2["default"])(_indexModule["default"]['test'], _styleModuleImportMap)
  }, "Hello");
};

var _default = Comp;
exports["default"] = _default;
