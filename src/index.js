import React from 'react'
import css from './index.module.css'

const Comp = () => {
    return (
        <button styleName={css['test']}>Hello</button>
    )
}

export default Comp
