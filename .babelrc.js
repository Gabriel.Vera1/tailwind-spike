const { generateScopedNameFactory } = require('@dr.pogodin/babel-plugin-react-css-modules/utils');
module.exports = {
    "presets": ['@babel/preset-react', '@babel/preset-env'],
    "plugins": [
       ["@dr.pogodin/react-css-modules", {
          "generateScopedName": generateScopedNameFactory("[path]___[name]__[local]___[hash:base64:5]"),
       }]

    ]
}
